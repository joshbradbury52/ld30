package uk.co.newagedev.LD30.levels;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.vector.Vector3f;

import uk.co.newagedev.LD30.blocks.BlockBridge;
import uk.co.newagedev.LD30.blocks.BlockTypes;
import uk.co.newagedev.LD30.entities.EntityPlayer;
import uk.co.newagedev.LD30.entities.EntityTypes;
import uk.co.newagedev.LD30.util.FlightPath;
import uk.co.newagedev.LD30.util.CollisionBox;

public class Level {

	private List<World> worlds = new ArrayList<World>();
	private List<BlockBridge> bridges = new ArrayList<BlockBridge>();
	private EntityPlayer player;
	private List<FlightPath> flightpaths = new ArrayList<FlightPath>();
	private FlightPath path;

	public void init() {
		
	}
	
	public void setWorld(World world, String[][][] worldData) {
		for (int x = 0; x < world.getDimensions().x; x++) {
			for (int y = 0; y < world.getDimensions().y; y++) {
				for (int z = 0; z < world.getDimensions().z; z++) {
					if (BlockTypes.isValid(worldData[x][y][z])) {
						world.setBlock(new Vector3f(x * 2, y * 2, z * 2), BlockTypes.getBlock(worldData[x][y][z]));
					} else if (EntityTypes.isValid(worldData[x][y][z])) {
						world.spawnEntity(new Vector3f(x * 2, y * 2, z * 2), EntityTypes.getEntity(worldData[x][y][z]));
					}
				}
			}
		}
		worlds.add(world);
	}

	public void setPlayer(EntityPlayer player) {
		this.player = player;
	}
	
	public EntityPlayer getPlayer() {
		return player;
	}

	public void render() {
		player.getCamera().view();
		
		for (World world : worlds) {
			world.render();
		}
		for (BlockBridge bridge : bridges) {
			bridge.render();
		}
		player.render();
	}
	
	public List<CollisionBox> getCollisionBoxes() {
		List<CollisionBox> boxes = new ArrayList<CollisionBox>();
		for (World world : worlds) {
			for (CollisionBox box : world.getCollisionBoxes()) {
				boxes.add(box);
			}
		}
		for (BlockBridge bridge : bridges) {
			boxes.add(bridge.getCollisionBox());
		}
		return boxes;
	}
	
	public World getWorldWithPlayerIn() {
		if (worlds.get(0).getBridgeComponents().size() > 0) {
			return worlds.get(0);
		} else {
			return worlds.get(1);
		}
	}
	
	public void addBridge(Vector3f loc) {
		BlockBridge bridge = new BlockBridge();
		bridge.setLocation(new Vector3f(loc.x * 2, loc.y * 2, loc.z * 2));
		bridges.add(bridge);
	}

	public boolean isCurrentPathRunning() {
		return path == null ? false : path.isRunning();
	}
	
	public void spawnFinish() {
		
	}
	
	public void addFlightPath(FlightPath path) {
		flightpaths.add(path);
	}
	
	public void startFlightPath(int i) {
		path = flightpaths.get(i);
		path.setCamera(player.getCamera());
		path.start();
	}
	
	public List<World> getWorlds() {
		return worlds;
	}
	
	public void update() {
		if (path != null) {
			if (path.isRunning()) {
				path.run();
			} else {
				path = null;
			}
		}
		for (World world : worlds) {
			world.update();
		}
		player.update();
	}
}
