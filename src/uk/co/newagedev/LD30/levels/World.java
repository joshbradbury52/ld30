package uk.co.newagedev.LD30.levels;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.vector.Vector3f;

import uk.co.newagedev.LD30.blocks.Block;
import uk.co.newagedev.LD30.blocks.BlockFinish;
import uk.co.newagedev.LD30.entities.Entity;
import uk.co.newagedev.LD30.entities.EntityBridgeComponent;
import uk.co.newagedev.LD30.entities.EntitySpawnBlocks;
import uk.co.newagedev.LD30.util.CollisionBox;

public class World {

	private List<Block> blocks = new ArrayList<Block>();
	private List<Entity> entities = new ArrayList<Entity>();
	private List<BlockFinish> finishBlocks = new ArrayList<BlockFinish>();
	private Vector3f location, dimensions;

	public void setLocation(Vector3f loc) {
		location = loc;
	}

	public void setDimensions(Vector3f dim) {
		dimensions = dim;
	}

	public Vector3f getLocation() {
		return location;
	}

	public Vector3f getDimensions() {
		return dimensions;
	}

	public List<EntityBridgeComponent> getBridgeComponents() {
		List<EntityBridgeComponent> comps = new ArrayList<EntityBridgeComponent>();
		for (Entity entity : entities) {
			if (entity instanceof EntityBridgeComponent) {
				comps.add((EntityBridgeComponent) entity);
			}
		}
		return comps;
	}
	
	public List<EntitySpawnBlocks> getSpawnBlocks() {
		List<EntitySpawnBlocks> blocks = new ArrayList<EntitySpawnBlocks>();
		for (Entity entity : entities) {
			if (entity instanceof EntitySpawnBlocks) {
				blocks.add((EntitySpawnBlocks) entity);
			}
		}
		return blocks;
	}

	public List<CollisionBox> getCollisionBoxes() {
		List<CollisionBox> boxes = new ArrayList<CollisionBox>();
		for (Block block : blocks) {
			if (block.getCollisionBox() != null) {
				boxes.add(block.getCollisionBox());
			}
		}
		for (Entity entity : entities) {
			if (entity.getCollisionBox() != null) {
				boxes.add(entity.getCollisionBox());
			}
		}
		return boxes;
	}
	
	public void addBlockFinish(Vector3f loc, BlockFinish block) {
		setBlock(loc, block);
		finishBlocks.add(block);
	}
	
	public List<BlockFinish> getFinishBlocks() {
		return finishBlocks;
	}
	
	public Vector3f getRelative(Vector3f loc) {
		return new Vector3f(loc.x + location.x, loc.y + location.y, loc.z + location.z);
	}

	public void setBlock(Vector3f loc, Block block) {
		blocks.add(block);
		block.setWorld(this);
		loc.x += location.x;
		loc.y += location.y;
		loc.z += location.z;
		block.setLocation(loc);
	}

	public void spawnEntity(Vector3f loc, Entity entity) {
		entities.add(entity);
		loc.x += location.x;
		loc.y += location.y;
		loc.z += location.z;
		entity.setLocation(loc);
	}

	public void move(Vector3f distance) {
		for (Block block : blocks) {
			block.move(distance);
		}
		for (Entity entity : entities) {
			entity.move(distance);
		}
	}

	public void update() {
		for (Block block : blocks) {
			block.update();
		}
		List<Entity> entitiesToRemove = new ArrayList<Entity>();
		for (Entity entity : entities) {
			entity.update();
			if (entity.shouldDie()) {
				entitiesToRemove.add(entity);
			}
		}
		for (Entity entity : entitiesToRemove) {
			entities.remove(entity);
		}
	}

	public void render() {
		for (Block block : blocks) {
			block.render();
		}
		for (Entity entity : entities) {
			entity.render();
		}
	}

}
