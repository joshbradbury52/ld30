package uk.co.newagedev.LD30.util;

import java.util.ArrayList;
import java.util.List;

import uk.co.newagedev.LD30.graphics.Camera;

public class FlightPath {

	private List<CameraGlideToLocation> locs = new ArrayList<CameraGlideToLocation>();
	private int currentPath = -1;
	private Camera camera;
	private boolean running = false;
	
	public void setCamera(Camera camera) {
		this.camera = camera;
	}
	
	public void addPath(CameraGlideToLocation loc) {
		locs.add(loc);
	}
	
	public boolean isRunning() {
		return running;
	}
	
	public void start() {
		if (currentPath < 0) {
			running = true;
			currentPath = 0;
			locs.get(0).setCamera(camera);
			locs.get(0).start();
		}
	}
	
	public void run() {
		if (currentPath >= 0) {
			if (locs.get(currentPath).isRunning()) {
				locs.get(currentPath).run();
			} else {
				currentPath++;
				if (currentPath < locs.size()) {
					locs.get(currentPath).setCamera(camera);
					locs.get(currentPath).start();
				} else {
					currentPath = -1;
					running = false;
				}
			}
		}
	}
}
