package uk.co.newagedev.LD30.util;

import org.lwjgl.util.vector.Vector3f;

import uk.co.newagedev.LD30.graphics.Camera;

public class CameraGlideToLocation {

	private Vector3f startLoc, startRot, locDiff, rotDiff;
	private float time = 0, timeToTake;
	private boolean running = false;
	private Camera camera;

	public CameraGlideToLocation(Vector3f startLoc, Vector3f endLoc, Vector3f startRot, Vector3f endRot, int timeToTake) {
		this.startLoc = startLoc;
		this.startRot = startRot;
		this.timeToTake = timeToTake;
		locDiff = new Vector3f(endLoc.x - startLoc.x, endLoc.y - startLoc.y, endLoc.z - startLoc.z);
		rotDiff = new Vector3f(endRot.x - startRot.x, endRot.y - startRot.y, endRot.z - startRot.z);
		
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	public void start() {
		if (!running) {
			running = true;
			camera.setLocation(startLoc);
			camera.setRotation(startRot);
		}
	}

	public boolean isRunning() {
		return running;
	}

	public void run() {
		if (running) {
			time++;
			if (time <= timeToTake) {
				float percentage = time / timeToTake;
				camera.setLocation(new Vector3f(startLoc.x + (locDiff.x * percentage), startLoc.y + (locDiff.y * percentage), startLoc.z + (locDiff.z * percentage)));
				camera.setRotation(new Vector3f(startRot.x + (rotDiff.x * percentage), startRot.y + (rotDiff.y * percentage), startRot.z + (rotDiff.z * percentage)));
			} else {
				time = 0;
				running = false;
			}
		}
	}

}
