package uk.co.newagedev.LD30.util;

import org.lwjgl.util.vector.Vector3f;

public class CollisionBox {

	private Vector3f position, dimensions;
	private boolean isSolid = true;
	
	public CollisionBox(Vector3f dimensions) {
		this.dimensions = dimensions;
	}
	
	public CollisionBox(Vector3f dimensions, boolean isSolid) {
		this.dimensions = dimensions;
		this.isSolid = isSolid;
	}
	
	public Vector3f getMinPos() {
		return new Vector3f(Math.min(position.x, position.x + dimensions.x), Math.min(position.y, position.y + dimensions.y), Math.min(position.z, position.z + dimensions.z));
	}
	
	public Vector3f getMaxPos() {
		return new Vector3f(Math.max(position.x, position.x + dimensions.x), Math.max(position.y, position.y + dimensions.y), Math.max(position.z, position.z + dimensions.z));
	}
	
	public void setLocation(Vector3f loc) {
		position = loc;
	}
	
	public void setDimensions(Vector3f dim) {
		dimensions = dim;
	}
	
	public Vector3f getLocation() {
		return position;
	}
	
	public Vector3f getDimensions() {
		return dimensions;
	}
	
	public void move(Vector3f relative) {
		position.x += relative.x;
		position.y += relative.y;
		position.z += relative.z;
	}
	
	public boolean isTouching(CollisionBox box) {
		return !(box.getMaxPos().x < getMinPos().x || getMaxPos().x < box.getMinPos().x || box.getMaxPos().y < getMinPos().y || getMaxPos().y < box.getMinPos().y || box.getMaxPos().z < getMinPos().z || getMaxPos().z < box.getMinPos().z);
	}
	
	public boolean isColliding(CollisionBox box) {
		return isSolid ? isTouching(box) : false;
	}
}
