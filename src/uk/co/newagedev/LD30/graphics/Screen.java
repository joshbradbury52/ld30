package uk.co.newagedev.LD30.graphics;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_LINES;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glDeleteTextures;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glScalef;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glTranslatef;
import static org.lwjgl.opengl.GL11.glVertex2f;
import static org.lwjgl.opengl.GL11.glVertex3f;
import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.util.glu.GLU.gluPerspective;

import java.util.HashMap;
import java.util.Map;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import uk.co.newagedev.LD30.LD30;

public class Screen {

	public Map<String, Sprite> sprites = new HashMap<String, Sprite>();
	
	public void resize(int width, int height) {
		glViewport(0, 0, width, height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(75.0f, width / height, 0.5f, 1000);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}
	
	public void renderLine(float startX, float startY, float startZ, float endX, float endY, float endZ) {
		glVertex3f(startX, startY, startZ);
		glVertex3f(endX, endY, endZ);
	}

	public void renderBox(float x, float y, float z, float xRotation, float yRotation, float zRotation, float scaleX, float scaleY, float scaleZ) {
		glPushMatrix();
		glTranslatef(x, y, z);

		glScalef(scaleX, scaleY, scaleZ);

		if (xRotation != 0)
			glRotatef(xRotation, 1.0f, 0.0f, 0.0f);
		if (yRotation != 0)
			glRotatef(yRotation, 0.0f, 1.0f, 0.0f);
		if (zRotation != 0)
			glRotatef(zRotation, 0.0f, 0.0f, 1.0f);

		glDisable(GL_TEXTURE_2D);
		
		glBegin(GL_LINES);

		renderLine(-1, -1, -1, 1, -1, -1);
		renderLine(-1, 1, -1, 1, 1, -1);
		renderLine(-1, -1, -1, -1, 1, -1);
		renderLine(1, -1, -1, 1, 1, -1);
		renderLine(-1, -1, -1, -1, -1, 1);
		renderLine(-1, 1, -1, -1, 1, 1);
		renderLine(1, -1, -1, 1, -1, 1);
		renderLine(1, 1, -1, 1, 1, 1);
		renderLine(-1, -1, 1, 1, -1, 1);
		renderLine(-1, 1, 1, 1, 1, 1);
		renderLine(-1, -1, 1, -1, 1, 1);
		renderLine(1, -1, 1, 1, 1, 1);
		
		glEnd();
		
		glEnable(GL_TEXTURE_2D);
		glPopMatrix();
	}
	
	public void loadSprite(String name, String path) {
		sprites.put(name, new Sprite(path));
	}
	
	public void init() {
		try {
			Display.setDisplayMode(new DisplayMode(800, 600));
			Display.setInitialBackground(0.0f, 0.0f, 0.0f);
			Display.setTitle("LD30");
			Display.create();
			glEnable(GL_DEPTH_TEST);
			glClearColor(127f / 255f, 137f / 255f, 229f / 255f, 1);

			loadSprite("gs", "/textures/grass-side.png");
			loadSprite("gt", "/textures/grass-top.png");
			loadSprite("d", "/textures/dirt.png");
			loadSprite("stone", "/textures/stone.png");
			loadSprite("pbf", "/textures/player/player-body-front.png");
			loadSprite("hf", "/textures/player/head-front.png");
			loadSprite("hs", "/textures/player/head-side.png");
			loadSprite("hba", "/textures/player/head-back.png");
			loadSprite("ht", "/textures/player/head-top.png");
			loadSprite("hbo", "/textures/player/head-bottom.png");
			loadSprite("as", "/textures/player/arm-side.png");
			loadSprite("at", "/textures/player/arm-top.png");
			loadSprite("ab", "/textures/player/arm-bottom.png");
			loadSprite("ls", "/textures/player/leg-side.png");
			loadSprite("lf", "/textures/player/leg-front.png");
			loadSprite("tt", "/textures/treasure-top.png");
			loadSprite("ts", "/textures/treasure-side.png");
			loadSprite("lo", "/textures/lock.png");
			loadSprite("ms", "/textures/metal-side.png");
			loadSprite("mt", "/textures/metal-top.png");
			loadSprite("finish", "/textures/finish.png");
			
			glEnable(GL_TEXTURE_2D);
			
			Mouse.setGrabbed(true);

			resize(800, 600);
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
	}
	
	public void renderFace(float xOffset, float yOffset, float zOffset, float rotationX, float rotationY, float rotationZ, float scaleX, float scaleY, float scaleZ, int textureID) {
		glPushMatrix();
		
		glTranslatef(xOffset, yOffset, zOffset);
		glScalef(scaleX, scaleY, scaleZ);
		glRotatef(rotationX, 1, 0, 0);
		glRotatef(rotationY, 0, 1, 0);
		glRotatef(rotationZ, 0, 0, 1);
		
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindTexture(GL_TEXTURE_2D, textureID);
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f(-1, -1);
		
		glTexCoord2f(1, 0);
		glVertex2f(1, -1);
		
		glTexCoord2f(1, 1);
		glVertex2f(1, 1);
		
		glTexCoord2f(0, 1);
		glVertex2f(-1, 1);
		glEnd();
		
		glPopMatrix();
	}
	
	public void renderCube(float xOffset, float yOffset, float zOffset, float rotationX, float rotationY, float rotationZ, float scaleX, float scaleY, float scaleZ, String[] textures) {
		glPushMatrix();

		glTranslatef(xOffset, yOffset, zOffset);
		
		glScalef(scaleX, scaleY, scaleZ);
		
		if (rotationX != 0)
			glRotatef(rotationX, 1.0f, 0.0f, 0.0f);
		if (rotationY != 0)
			glRotatef(rotationY, 0.0f, 1.0f, 0.0f);
		if (rotationZ != 0)
			glRotatef(rotationZ, 0.0f, 0.0f, 1.0f);
		
		renderFace(-1, 0, 0, 180, 90, 0, 1, 1, 1, sprites.get(textures[0]).getTexture());
		renderFace(1, 0, 0, 180, 90, 0, 1, 1, 1, sprites.get(textures[1]).getTexture());
		renderFace(0, -1, 0, 90, 0, 270, 1, 1, 1, sprites.get(textures[2]).getTexture());
		renderFace(0, 1, 0, 90, 0, 270, 1, 1, 1, sprites.get(textures[3]).getTexture());
		renderFace(0, 0, -1, 0, 0, 180, 1, 1, 1, sprites.get(textures[4]).getTexture());
		renderFace(0, 0, 1, 0, 0, 180, 1, 1, 1, sprites.get(textures[5]).getTexture());
		glPopMatrix();
	}

	public void render() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		LD30.getCurrentLevel().render();
	}
	
	public void run() {
		Display.sync(120);
		Display.update();
	}
	
	public boolean isCloseRequested() {
		return Display.isCloseRequested();
	}
	
	public void clearUp() {
		for (Sprite sprite : sprites.values()) {
			glDeleteTextures(sprite.getTexture());
		}
		Display.destroy();
	}
	
}
