package uk.co.newagedev.LD30.graphics;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

public class Camera {

	private float yaw, pitch, roll;
	private Vector3f position;

	public Camera(float x, float y, float z) {
		this.yaw = 0f;
		this.pitch = 0f;
		this.roll = 0f;
		position = new Vector3f(x, y, z);
	}

	public Vector3f getLocation() {
		return position;
	}
	
	public void setLocation(Vector3f loc) {
		position = loc;
	}
	
	public void setRotation(Vector3f rot) {
		yaw = rot.x;
		pitch = rot.y;
		roll = rot.z;
	}
	
	public void move(float x, float y, float z) {
		position.x += x;
		position.y += y;
		position.z += z;
	}

	public void yaw(float amount) {
		yaw += amount;
	}

	public void pitch(float amount) {
		pitch += amount;
	}

	public void roll(float amount) {
		roll += amount;
	}

	public float getYaw() {
		return yaw;
	}

	public float getPitch() {
		return pitch;
	}

	public float getRoll() {
		return roll;
	}

	public void view() {
		GL11.glLoadIdentity();
		GL11.glRotatef(-pitch, 1, 0, 0);
		GL11.glRotatef(yaw, 0, 1, 0);
		GL11.glRotatef(roll, 0, 0, 1);
		GL11.glTranslatef(position.x, -position.y, position.z);
	}

	public void moveForward(float distance) {
		position.x -= distance * (float) Math.sin(Math.toRadians(yaw));
		position.z += distance * (float) Math.cos(Math.toRadians(yaw));
	}

	public void moveBack(float distance) {
		position.x += distance * (float) Math.sin(Math.toRadians(yaw));
		position.z -= distance * (float) Math.cos(Math.toRadians(yaw));
	}

	public void moveLeft(float distance) {
		position.x -= distance * (float) Math.sin(Math.toRadians(yaw - 90));
		position.z += distance * (float) Math.cos(Math.toRadians(yaw - 90));
	}

	public void moveRight(float distance) {
		position.x -= distance * (float) Math.sin(Math.toRadians(yaw + 90));
		position.z += distance * (float) Math.cos(Math.toRadians(yaw + 90));
	}
}
