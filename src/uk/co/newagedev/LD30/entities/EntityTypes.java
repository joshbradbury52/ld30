package uk.co.newagedev.LD30.entities;

public enum EntityTypes {

	BRIDGE(new EntityBridgeComponent(), "b");
	
	private Entity entity;
	private String type;
	
	EntityTypes(Entity entity, String type) {
		this.entity = entity;
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
	
	public Entity getEntity() {
		return entity.getInstance();
	}
	
	public static boolean isValid(String type) {
		for (EntityTypes t : values()) {
			if (t.type == type) {
				return true;
			}
		}
		return false;
	}
	
	public static Entity getEntity(String type) {
		for (EntityTypes t : values()) {
			if (t.getType() == type) {
				return t.getEntity();
			}
		}
		return null;
	}
}
