package uk.co.newagedev.LD30.entities;

import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glScalef;
import static org.lwjgl.opengl.GL11.glTranslatef;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;

import uk.co.newagedev.LD30.LD30;
import uk.co.newagedev.LD30.blocks.BlockFinish;
import uk.co.newagedev.LD30.graphics.Camera;
import uk.co.newagedev.LD30.util.CollisionBox;
import uk.co.newagedev.LD30.sounds.Sounds;

public class EntityPlayer extends Entity {

	private Camera camera = new Camera(-5f, 2f, 0f);
	private int endCooldown = -1;

	public EntityPlayer() {
		setCollisionBox(new CollisionBox(new Vector3f(1f, 4.5f, 1.75f)));
	}

	public void setColAndLoc() {
		CollisionBox box = new CollisionBox(new Vector3f(1f, 4.5f, 1.75f));
		box.setLocation(getLocation());
		setCollisionBox(box);
	}

	public void render() {
		setColAndLoc();
		glPushMatrix();

		glTranslatef(getLocation().x - 0.7f, getLocation().y + 1f, getLocation().z);
		glRotatef(getRotation().x, 1, 0, 0);
		glRotatef(-getRotation().y, 0, 1, 0);
		glRotatef(getRotation().z, 0, 0, 1);

		glScalef(0.75f, 0.75f, 0.75f);

		if (LD30.getCurrentLevel().isCurrentPathRunning())
			LD30.getScreen().renderCube(0f, 2.3f, 0f, 0, 0, 0, 0.5f, 0.6f, 0.5f, new String[] { "hba", "hf", "hbo", "ht", "hs", "hs" });// head
		LD30.getScreen().renderCube(0, 0.4f, 0, 0, 0, 0, LD30.getCurrentLevel().isCurrentPathRunning() ? 0.4f : 0.2f, 1.3f, 0.7f, new String[] { "pbf", "pbf", "pbf", "pbf", "pbf", "pbf" });// chest
		LD30.getScreen().renderCube(0f, 0.4f, -1f, 0, 0, 0, LD30.getCurrentLevel().isCurrentPathRunning() ? 0.3f : 0.15f, 1.2f, 0.3f, new String[] { "as", "as", "ab", "at", "as", "as" });// larm
		LD30.getScreen().renderCube(0f, 0.4f, 1f, 0, 0, 0, LD30.getCurrentLevel().isCurrentPathRunning() ? 0.3f : 0.15f, 1.2f, 0.3f, new String[] { "as", "as", "ab", "at", "as", "as" });// rarm
		LD30.getScreen().renderCube(0f, -2f, 0.4f, 0, 0, 0, 0.3f, 1.1f, 0.3f, new String[] { "ls", "lf", "d", "ab", "ls", "ls" });// rleg
		LD30.getScreen().renderCube(0f, -2f, -0.4f, 0, 0, 0, 0.3f, 1.1f, 0.3f, new String[] { "ls", "lf", "d", "ab", "ls", "ls" });// lleg
		glPopMatrix();
	}

	@Override
	public void setLocation(Vector3f location) {
		super.setLocation(location);
		if (LD30.getCurrentLevel() != null && !LD30.getCurrentLevel().isCurrentPathRunning()) {
			camera.setLocation(new Vector3f(location.x - 0.2f, location.y + 1.75f, location.z - 0.2f));
		}
	}

	@Override
	public void move(Vector3f loc) {
		super.move(loc);
		if (LD30.getCurrentLevel() != null && !LD30.getCurrentLevel().isCurrentPathRunning()) {
			camera.setLocation(new Vector3f(-(getLocation().x - 0.7f), getLocation().y + 3f, -(getLocation().z)));
		}
	}

	public Camera getCamera() {
		return camera;
	}

	public void update() {
		super.update();
		float dy = Mouse.getDY();
		if (!LD30.getCurrentLevel().isCurrentPathRunning()) {
			if (camera.getPitch() + (dy * 0.1f) >= -45 && camera.getPitch() + (dy * 0.1f) < 90)
				camera.pitch(dy * 0.1f);
			camera.setRotation(new Vector3f(getRotation().y + 90, camera.getPitch(), camera.getRoll()));
			rotate(new Vector3f(0, Mouse.getDX() * 0.1f, 0));

			if (Keyboard.isKeyDown(Keyboard.KEY_SPACE))
				startJump();
			if (Keyboard.isKeyDown(Keyboard.KEY_D) || Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
				move(new Vector3f(-(0.1f * (float) Math.sin(Math.toRadians(getRotation().y))), 0, 0));
				move(new Vector3f(0, 0, 0.1f * (float) Math.cos(Math.toRadians(getRotation().y))));
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_W) || Keyboard.isKeyDown(Keyboard.KEY_UP)) {
				move(new Vector3f(-(0.1f * (float) Math.sin(Math.toRadians(getRotation().y - 90))), 0, 0));
				move(new Vector3f(0, 0, 0.1f * (float) Math.cos(Math.toRadians(getRotation().y - 90))));
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_A) || Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
				move(new Vector3f(0.1f * (float) Math.sin(Math.toRadians(getRotation().y)), 0, 0));
				move(new Vector3f(0, 0, -(0.1f * (float) Math.cos(Math.toRadians(getRotation().y)))));
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_S) || Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
				move(new Vector3f(-(0.1f * (float) Math.sin(Math.toRadians(getRotation().y + 90))), 0, 0));
				move(new Vector3f(0, 0, 0.1f * (float) Math.cos(Math.toRadians(getRotation().y + 90))));
			}

			if (LD30.getCurrentLevel().getWorldWithPlayerIn() != null) {
				for (EntityBridgeComponent comp : LD30.getCurrentLevel().getWorldWithPlayerIn().getBridgeComponents()) {
					if (comp.getCollisionBox().isTouching(getCollisionBox())) {
						Sounds.PICKUP.play(getLocation().x, getLocation().y, getLocation().z);
						if (LD30.getCurrentLevel().getWorlds().get(0) == LD30.getCurrentLevel().getWorldWithPlayerIn()) {
							for (int j = -2; j < 1; j++) {
								for (int i = 0; i < 10; i++) {
									LD30.getCurrentLevel().addBridge(new Vector3f(-1 - (9 * LD30.getCurrentLevel().getWorldWithPlayerIn().getBridgeComponents().size()) + i, 2.25f, 5 + j));
								}
							}
						} else {
							if (LD30.getCurrentLevel().getWorldWithPlayerIn().getBridgeComponents().size() <= 1) {
								LD30.getCurrentLevel().spawnFinish();
							}
						}
						comp.remove();
					}
				}
				for (EntitySpawnBlocks block : LD30.getCurrentLevel().getWorldWithPlayerIn().getSpawnBlocks()) {
					if (block.getCollisionBox().isTouching(getCollisionBox())) {
						block.runSpawn();
						Sounds.PICKUP.play(getLocation().x, getLocation().y, getLocation().z);
						block.remove();
					}
				}
				for (BlockFinish block : LD30.getCurrentLevel().getWorldWithPlayerIn().getFinishBlocks()) {
					if (endCooldown < 0) {
						if (block.getCollisionBox().isTouching(getCollisionBox())) {
							Sounds.END.play(getLocation().x, getLocation().y, getLocation().z);
							endCooldown = 100;
						}
					}
				}
			}

			if (endCooldown > 0) {
				endCooldown--;
			}

			if (endCooldown == 0) {
				LD30.running = false;
			}

			move(new Vector3f(0, -0.1f, 0));
		}
	}
}
