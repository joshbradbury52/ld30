package uk.co.newagedev.LD30.entities;

import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glTranslatef;

import org.lwjgl.util.vector.Vector3f;

import uk.co.newagedev.LD30.LD30;
import uk.co.newagedev.LD30.util.CollisionBox;

public class EntityBridgeComponent extends Entity {

	public EntityBridgeComponent() {
		setCollisionBox(new CollisionBox(new Vector3f(2, 2, 2), false));
	}
	
	public void setColAndLoc() {
		CollisionBox box = new CollisionBox(new Vector3f(2, 2, 2), false);
		box.setLocation(getLocation());
		setCollisionBox(box);
	}

	public void render() {
		setColAndLoc();
		glPushMatrix();
		glTranslatef(getLocation().x, getLocation().y, getLocation().z);
		glRotatef(getRotation().x, 1, 0, 0);
		glRotatef(-getRotation().y, 0, 1, 0);
		glRotatef(getRotation().z, 0, 0, 1);

		LD30.getScreen().renderCube(0, 0, 0, 0, 0, 0, 1, 1, 1, new String[] { "ts", "ts", "ts", "tt", "ts", "ts" });
		LD30.getScreen().renderCube(1, 0.3f, 0f, 0, 0, 0, 0.1f, 0.4f, 0.3f, new String[] { "stone", "lo", "stone", "stone", "stone", "stone" });
		LD30.getScreen().renderCube(0, 1, 0, 0, 0, 0, 0.9f, 0.1f, 0.95f, new String[] { "tt", "tt", "ts", "tt", "ts", "ts" });
		LD30.getScreen().renderCube(0, 1.1f, 0, 0, 0, 0, 0.8f, 0.1f, 0.9f, new String[] { "tt", "tt", "ts", "tt", "ts", "ts" });
		LD30.getScreen().renderCube(0, 1.2f, 0, 0, 0, 0, 0.7f, 0.1f, 0.85f, new String[] { "tt", "tt", "ts", "tt", "ts", "ts" });
		LD30.getScreen().renderCube(0, 1.3f, 0, 0, 0, 0, 0.6f, 0.1f, 0.75f, new String[] { "tt", "tt", "ts", "tt", "ts", "ts" });
		glPopMatrix();
	}
	
	private float counter = 0;
	
	public void update() {
		rotate(new Vector3f(0, 0.1f, 0));
		getLocation().y += (Math.sin(counter) / 100);
		counter += 0.05f;
	}
}
