package uk.co.newagedev.LD30.entities;

import org.lwjgl.util.vector.Vector3f;

import uk.co.newagedev.LD30.LD30;
import uk.co.newagedev.LD30.util.CollisionBox;
import uk.co.newagedev.LD30.sounds.Sounds;

public abstract class Entity {

	private Vector3f location, rotation = new Vector3f(0, 0, 0), scale = new Vector3f(1, 1, 1);
	private int jumpTimer = 0;
	private CollisionBox box;
	private boolean die = false;

	public void setCollisionBox(CollisionBox box) {
		this.box = box;
	}

	public CollisionBox getCollisionBox() {
		return box;
	}

	public void setLocation(Vector3f loc) {
		getCollisionBox().setLocation(loc);
		location = loc;
	}

	public void setRotation(Vector3f rot) {
		rotation = rot;
	}

	public void setScale(Vector3f sca) {
		scale = sca;
	}

	public Vector3f getLocation() {
		return location;
	}

	public Vector3f getRotation() {
		return rotation;
	}

	public Vector3f getScale() {
		return scale;
	}

	public boolean isColliding() {
		if (this.box != null) {
			for (CollisionBox box : LD30.getCurrentLevel().getCollisionBoxes()) {
				if (box != getCollisionBox()) {
					if (box.isColliding(getCollisionBox())) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public void startJump() {
		if (jumpTimer < 10) {
			Sounds.JUMP.play(getLocation().x, getLocation().y, getLocation().z);
			jumpTimer++;
		}
	}

	public void move(Vector3f loc) {
		location.x += loc.x;
		location.y += loc.y;
		location.z += loc.z;
		if (isColliding()) {
			if (loc.x == 0 && loc.z == 0 && jumpTimer > 10)
				jumpTimer = 0;
			location.x -= loc.x;
			location.y -= loc.y;
			location.z -= loc.z;
		}
	}

	public void remove() {
		die = true;
	}

	public boolean shouldDie() {
		return die;
	}

	public void rotate(Vector3f rot) {
		rotation.x += rot.x;
		rotation.y += rot.y;
		rotation.z += rot.z;
	}

	public void scale(Vector3f sca) {
		scale.x += sca.x;
		scale.y += sca.y;
		scale.z += sca.z;
	}

	public void update() {
		if (jumpTimer > 0) {
			jumpTimer++;
			if (jumpTimer < 140) {
				move(new Vector3f(0, (0.8f - (jumpTimer / 100f)) * 0.5f, 0));
			}
		}
	}

	public Entity getInstance() {
		Entity entity = null;
		try {
			entity = this.getClass().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return entity;
	}

	public abstract void render();

}
