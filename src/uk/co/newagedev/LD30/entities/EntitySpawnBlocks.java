package uk.co.newagedev.LD30.entities;

import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glTranslatef;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.vector.Vector3f;

import uk.co.newagedev.LD30.LD30;
import uk.co.newagedev.LD30.util.CollisionBox;
import uk.co.newagedev.LD30.blocks.Block;

public class EntitySpawnBlocks extends Entity {

	private List<Block> blocksToAdd = new ArrayList<Block>();
	
	public EntitySpawnBlocks(List<Block> blocksToAdd) {
		setCollisionBox(new CollisionBox(new Vector3f(2, 2, 2), false));
		this.blocksToAdd = blocksToAdd;
	}
	
	public void setColAndLoc() {
		CollisionBox box = new CollisionBox(new Vector3f(2, 2, 2), false);
		box.setLocation(getLocation());
		setCollisionBox(box);
	}
	
	public void runSpawn() {
		for (Block block : blocksToAdd) {
			LD30.getCurrentLevel().getWorldWithPlayerIn().setBlock(block.getLocation(), block);
		}
	}

	public void render() {
		setColAndLoc();
		glPushMatrix();
		glTranslatef(getLocation().x, getLocation().y, getLocation().z);
		glRotatef(getRotation().x, 1, 0, 0);
		glRotatef(-getRotation().y, 0, 1, 0);
		glRotatef(getRotation().z, 0, 0, 1);

		LD30.getScreen().renderCube(0, 0, 0, 0, 0, 0, 1, 1, 1, new String[] { "ms", "ms", "ms", "mt", "ms", "ms" });
		LD30.getScreen().renderCube(1, 0.3f, 0f, 0, 0, 0, 0.1f, 0.4f, 0.3f, new String[] { "stone", "lo", "stone", "stone", "stone", "stone" });
		LD30.getScreen().renderCube(0, 1, 0, 0, 0, 0, 0.9f, 0.1f, 0.95f, new String[] { "mt", "mt", "ms", "mt", "ms", "ms" });
		LD30.getScreen().renderCube(0, 1.1f, 0, 0, 0, 0, 0.8f, 0.1f, 0.9f, new String[] { "mt", "mt", "ms", "mt", "ms", "ms" });
		LD30.getScreen().renderCube(0, 1.2f, 0, 0, 0, 0, 0.7f, 0.1f, 0.85f, new String[] { "mt", "mt", "ms", "mt", "ms", "ms" });
		LD30.getScreen().renderCube(0, 1.3f, 0, 0, 0, 0, 0.6f, 0.1f, 0.75f, new String[] { "mt", "mt", "ms", "mt", "ms", "ms" });
		glPopMatrix();
	}
	
	private float counter = 0;
	
	public void update() {
		rotate(new Vector3f(0, 0.1f, 0));
		getLocation().y += (Math.sin(counter) / 100);
		counter += 0.05f;
	}
}
