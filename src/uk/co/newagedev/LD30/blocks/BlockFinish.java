package uk.co.newagedev.LD30.blocks;

import org.lwjgl.util.vector.Vector3f;

import uk.co.newagedev.LD30.util.CollisionBox;

public class BlockFinish extends Block {

	public BlockFinish() {
		super("finish");
		setCollisionBox(new CollisionBox(new Vector3f(2, 2, 2), false));
	}
	
}
