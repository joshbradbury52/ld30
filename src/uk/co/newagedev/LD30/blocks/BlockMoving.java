package uk.co.newagedev.LD30.blocks;

import org.lwjgl.util.vector.Vector3f;

import uk.co.newagedev.LD30.LD30;
import uk.co.newagedev.LD30.sounds.Sounds;

public class BlockMoving extends Block {

	private Vector3f startLoc = new Vector3f(0, 0, 0), endLoc = new Vector3f(0, 0, 0), moveAmount;
	private boolean moveForward = true, moving = true;
	private int cooldown = 0, resetCooldown = 0;

	public BlockMoving() {
		super("stone");
	}

	public void setEndLocation(Vector3f loc) {
		endLoc = getWorld().getRelative(loc);
		moveAmount = new Vector3f((endLoc.x - startLoc.x) / 1000f, (endLoc.y - startLoc.y) / 1000f, (endLoc.z - startLoc.z) / 1000f);
	}
	
	public void setLocation(Vector3f loc) {
		super.setLocation(loc);
		startLoc = new Vector3f(loc.x, loc.y, loc.z);
		moveAmount = new Vector3f((endLoc.x - startLoc.x) / 1000f, (endLoc.y - startLoc.y) / 1000f, (endLoc.z - startLoc.z) / 1000f);
	}

	public void moveForward() {
		if (moveForward && moving) {
			if (cooldown < 1 && resetCooldown < 1) {
				if (Math.abs(getLocation().x - endLoc.x) < 0.05f && Math.abs( getLocation().y - endLoc.y) < 0.05f && Math.abs(getLocation().z - endLoc.z) < 0.05f) {
					moving = false;
					resetCooldown = 300;
					cooldown = 240;
				} else {
					Vector3f playerLoc = LD30.getCurrentLevel().getPlayer().getLocation();
					if (Math.abs(getLocation().x - playerLoc.x) < 1f && playerLoc.y - getLocation().y < 3 && playerLoc.y - getLocation().y > 2 && Math.abs(getLocation().z - playerLoc.z) < 1f) LD30.getCurrentLevel().getPlayer().move(new Vector3f(moveAmount.x * 2, moveAmount.y * 2, moveAmount.z * 2));
					move(moveAmount);
				}
			}
		}
	}
	
	public void moveBack() {
		if (!moveForward && moving) {
			if (cooldown < 1 && resetCooldown < 1) {
				if (Math.abs(getLocation().x - startLoc.x) < 0.05f && Math.abs(getLocation().y - startLoc.y) < 0.05f && Math.abs(getLocation().z - startLoc.z) < 0.05f) {
					moving = false;
					resetCooldown = 300;
					cooldown = 240;
				} else {
					Vector3f playerLoc = LD30.getCurrentLevel().getPlayer().getLocation();
					if (Math.abs(getLocation().x - playerLoc.x) < 1f && playerLoc.y - getLocation().y < 3 && playerLoc.y - getLocation().y > 2 && Math.abs(getLocation().z - playerLoc.z) < 1f) LD30.getCurrentLevel().getPlayer().move(new Vector3f(-moveAmount.x * 2, -moveAmount.y * 2, -moveAmount.z * 2));
					move(new Vector3f(-moveAmount.x, -moveAmount.y, -moveAmount.z));
				}
			}
		}
	}
	
	public void update() {
		if (moving) {
			if (moveForward) {
				moveForward();
			} else {
				moveBack();
			}
		} else {
			moveForward = !moveForward;
			Sounds.MOVING_BLOCK.play(getLocation().x, getLocation().y, getLocation().z);
			moving = true;
		}
		if (cooldown > 0) {
			cooldown--;
		}
		if (resetCooldown > 0) {
			resetCooldown--;
		}
	}

}
