package uk.co.newagedev.LD30.blocks;

public enum BlockTypes {

	DIRT(new BlockDirt(), "d"),
	MOVING(new BlockMoving(), "m"),
	GRASS(new BlockGrass(), "g");
	
	private Block block;
	private String type;
	
	BlockTypes(Block block, String type) {
		this.block = block;
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
	
	public Block getBlock() {
		return block.getInstance();
	}
	
	public static boolean isValid(String type) {
		for (BlockTypes t : values()) {
			if (t.type == type) {
				return true;
			}
		}
		return false;
	}
	
	public static Block getBlock(String type) {
		for (BlockTypes t : BlockTypes.values()) {
			if (t.getType() == type) {
				return t.getBlock();
			}
		}
		return null;
	}
}
