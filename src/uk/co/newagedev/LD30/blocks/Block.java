package uk.co.newagedev.LD30.blocks;

import org.lwjgl.util.vector.Vector3f;

import uk.co.newagedev.LD30.LD30;
import uk.co.newagedev.LD30.util.CollisionBox;
import uk.co.newagedev.LD30.levels.World;

public class Block {

	private Vector3f location, rotation = new Vector3f(0, 0, 0), scale = new Vector3f(1, 1, 1);
	private String[] textures;
	private World world;
	private CollisionBox box;
	
	public void setCollisionBox(CollisionBox box) {
		this.box = box;
	}
	
	public void setWorld(World world) {
		this.world = world;
	}
	
	public World getWorld() {
		return world;
	}
	
	public CollisionBox getCollisionBox() {
		return box;
	}
	
	public Block(String[] textures) {
		this.textures = textures;
		setCollisionBox(new CollisionBox(new Vector3f(2, 2, 2)));
	}
	
	public Block(String texture) {
		textures = new String[] {texture, texture, texture, texture, texture, texture};
		setCollisionBox(new CollisionBox(new Vector3f(2, 2, 2)));
	}
	
	public void setLocation(Vector3f loc) {
		if (getCollisionBox() != null) getCollisionBox().setLocation(loc);
		location = loc;
	}
	
	public Vector3f getLocation() {
		return location;
	}
	
	public void move(Vector3f loc) {
		location.x += loc.x;
		location.y += loc.y;
		location.z += loc.z;
		getCollisionBox().move(loc);
	}
	
	public void render() {
		LD30.getScreen().renderCube(location.x, location.y, location.z, rotation.x, rotation.y, rotation.z, scale.x, scale.y, scale.z, textures);
	}
	
	public void update() {
		
	}
	
	public Block getInstance() {
		Block block = null;
		try {
			block = this.getClass().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return block;
	}
}
