package uk.co.newagedev.LD30.sounds;

import paulscode.sound.SoundSystemConfig;

import uk.co.newagedev.LD30.LD30;

public enum Sounds {

	JUMP("/sounds/jump.wav", "jump.wav"),
	END("/sounds/end.wav", "end.wav"),
	MOVING_BLOCK("/sounds/moving-block.wav", "moving-block.wav"),
	PICKUP("/sounds/pickup.wav", "pickup.wav"),
	PLACE("/sounds/place.wav", "place.wav");
	
	private String path, name;
	
	Sounds(String path, String name) {
		this.path = path;
		this.name = name;
	}
	
	public void play(float x, float y, float z) {
		LD30.soundSystem.quickPlay(false, Sounds.class.getResource(path), name, false, x, y, z, SoundSystemConfig.ATTENUATION_ROLLOFF, SoundSystemConfig.getDefaultRolloff()); 
	}
}
