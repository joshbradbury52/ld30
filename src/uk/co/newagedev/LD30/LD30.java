package uk.co.newagedev.LD30;

import org.lwjgl.input.Keyboard;

import paulscode.sound.SoundSystem;
import paulscode.sound.SoundSystemConfig;
import paulscode.sound.SoundSystemException;
import paulscode.sound.codecs.CodecWav;
import paulscode.sound.libraries.LibraryLWJGLOpenAL;
import uk.co.newagedev.LD30.graphics.Screen;
import uk.co.newagedev.LD30.levels.Level;
import uk.co.newagedev.LD30.levels.TestLevel;

public class LD30 {

	public static Screen screen;
	public static SoundSystem soundSystem;
	public static Level level = new TestLevel();
	public static boolean running = true;

	public static Screen getScreen() {
		return screen;
	}

	public static Level getCurrentLevel() {
		return level;
	}

	public void init() {
		new LD30();
		screen = new Screen();
		screen.init();
		try {
			SoundSystemConfig.addLibrary(LibraryLWJGLOpenAL.class);
			SoundSystemConfig.setCodec("wav", CodecWav.class);
		} catch (SoundSystemException e) {
			System.err.println("error linking with the plug-ins");
		}
		soundSystem = new SoundSystem();
	}

	public void run() {
		level.init();
		while (!screen.isCloseRequested() && !Keyboard.isKeyDown(Keyboard.KEY_ESCAPE) && running) {
			update();
			screen.render();
			screen.run();
		}
	}

	public void update() {
		level.update();
	}

	public static void main(String[] args) {
		LD30 main = new LD30();
		main.init();
		main.run();
		getScreen().clearUp();
		soundSystem.cleanup();
	}

}
